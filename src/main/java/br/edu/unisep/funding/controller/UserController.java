package br.edu.unisep.funding.controller;

import br.edu.unisep.funding.domain.usecase.user.GetUserByIdUseCase;
import br.edu.unisep.funding.domain.dto.UserDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {

    private final GetUserByIdUseCase getUserById;

    @GetMapping("/{id}")
    public ResponseEntity<UserDto> findById(@PathVariable("id") Integer id) {
        var result = getUserById.execute(id);
        return ResponseEntity.ok(result);
    }

}
