package br.edu.unisep.funding.domain.usecase.user;

import br.edu.unisep.funding.data.repository.UserRepository;
import br.edu.unisep.funding.domain.dto.UserDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GetUserByIdUseCase {

    private final UserRepository repository;

    public UserDto execute(Integer id) {
        var user = repository.findById(id).get();
        return new UserDto(user.getId(), user.getLogin(), user.getName(), null);
    }

}
