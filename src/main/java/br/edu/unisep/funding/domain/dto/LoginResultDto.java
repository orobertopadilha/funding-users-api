package br.edu.unisep.funding.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class LoginResultDto {

    private final UserDto userData;

    private final String token;

}
