package br.edu.unisep.funding.domain.usecase.auth;

import br.edu.unisep.funding.data.repository.UserRepository;
import br.edu.unisep.funding.security.data.UserAuthDetails;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import static br.edu.unisep.funding.utils.Messages.MESSAGE_INVALID_LOGIN;

@Service
@AllArgsConstructor
public class AuthUseCase implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        var user = userRepository.findByLogin(login);

        if (user.isPresent()) {
            return UserAuthDetails.from(user.get());
        } else {
            throw new UsernameNotFoundException(MESSAGE_INVALID_LOGIN);
        }
    }
}
