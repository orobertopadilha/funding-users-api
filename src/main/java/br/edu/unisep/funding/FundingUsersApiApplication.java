package br.edu.unisep.funding;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FundingUsersApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(FundingUsersApiApplication.class, args);
	}

}
