package br.edu.unisep.funding.security.jwt;

import br.edu.unisep.funding.domain.dto.UserDto;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@Component
public class JwtProvider {

    private static final String USER_KEY = "user";
    private static final String TOKEN_KEY = "!#@!#S1st3m45_2020_##&%";

    public static String createJwt(UserDto userData) {

        var issuedAt = LocalDateTime.now();
        var expiration = LocalDateTime.now().plusMinutes(10);

        var claims = Jwts.claims();
        claims.setSubject(userData.getId().toString());
        claims.setIssuedAt(Date.from(issuedAt.atZone(ZoneId.systemDefault()).toInstant()));
        claims.setExpiration(Date.from(expiration.atZone(ZoneId.systemDefault()).toInstant()));
        claims.put(USER_KEY, userData);

        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, TOKEN_KEY)
                .compact();
    }

    public static boolean isValid(String token) {
        try {
            Jwts.parser()
                    .setSigningKey(TOKEN_KEY)
                    .parseClaimsJws(token);
        } catch (Exception exception) {
            return false;
        }

        return true;
    }

    public static Claims getClaims(String token) {
        return Jwts.parser()
                .setSigningKey(TOKEN_KEY)
                .parseClaimsJws(token)
                .getBody();
    }

}
